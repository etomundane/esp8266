
const int trigPin=3;
const int echoPin=2;

// defines variables
long duration;
int distance;

void setup(){
    pinMode(trigPin,OUTPUT);
    pinMode(echoPin,INPUT);
    Serial.begin(9600);
}

void loop(){
// Clears the trigPin
digitalWrite(4, LOW);
digitalWrite(5, LOW);
digitalWrite(6, LOW);


digitalWrite(trigPin, LOW);
delayMicroseconds(2);
// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPin, HIGH);
delayMicroseconds(10);
digitalWrite(trigPin, LOW);
// Reads the echoPin, returns the sound wave travel time in microseconds
duration = pulseIn(echoPin, HIGH);
// Calculating the distance
distance= (duration*0.034)/2;
// Prints the distance on the Serial Monitor
Serial.print("Distance: ");
Serial.print(distance);
Serial.println(" cm");
switchLight();

delay(1000);
}

void switchLight() {
  if (distance > 10 && distance <= 20) {
    digitalWrite(4, HIGH);
    } else if (distance <= 30 ) {
      digitalWrite(4, HIGH);
      digitalWrite(5, HIGH);
      } else {
      digitalWrite(4, HIGH);
      digitalWrite(5, HIGH);
      digitalWrite(6, HIGH);
        }
}
