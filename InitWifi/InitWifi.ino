void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  // Mandatory at first line
  setUpWifi();
  setupMqtt();
  setupSensorTemperature();

  // simulate diff board
  setupRelays();
}

void loop() {
  // Mandatory at first line
  checkResetMode();
  listenMqttSubscribtion();
  listenTemperature();
}
