#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <ArduinoJson.h>

char* ssid = "DEFAULT"; //not used
char* password = "abc12345";
char* mySsid = "Smart Device";

IPAddress local_ip(192,168,5,11);
IPAddress gateway(192,168,5,1);
IPAddress netmask(255,255,255,0);

ESP8266WebServer server(80);

bool reset_flag = true;
uint16_t time_elapsed = 0;

void setUpWifi() {
  reset_flag = true;
  time_elapsed = 0;
    
  SPIFFS.begin();
  clearSavedWifi();
  server.on("/setup", HTTP_POST, persistWifi);
  server.begin();
}

void checkResetMode() {
  if(reset_flag) {
    recheckWifi();
    uint16_t time_start = millis();
    while(time_elapsed < 30000) {
      server.handleClient();
      Serial.println("Listen");
      time_elapsed = millis()-time_start;
      digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      delay(100);
    }
   reset_flag = false;
  } else {
    Serial.println("Stop");
    stopAP();
  }
}

void recheckWifi() {
  stopAP();
  getSavedWifi();
  if (WiFi.status() == WL_CONNECTED) {
      Serial.println("Connected to Network");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      delay(3000);
    } else {
      startAP();
    }
}

void persistWifi() {
  Serial.println("Handle Persist WIFI");
  String data = server.arg("plain");
  DynamicJsonDocument doc(512);
  DeserializationError error = deserializeJson(doc, data);

  if(!error) {
    File configFile = SPIFFS.open("/config.json", "w");
    serializeJson(doc, configFile);  
    configFile.close();
    basicResponse(true);
    delay(500);
    
    recheckWifi(); 
  }
}

void clearSavedWifi() {
  Serial.println("");
  String result = SPIFFS.remove("/config.json") ? "Wifi Cleared" : "Failed Clear Wifi";
  Serial.println(result);
}

void startAP() {
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(local_ip, gateway, netmask);
  WiFi.softAP(mySsid, password);
  Serial.println("");
  WiFi.printDiag(Serial);
}

void stopAP() {
  WiFi.softAPdisconnect(true);
  WiFi.disconnect();          
  delay(1000);
  Serial.println("");
  WiFi.printDiag(Serial);
}

void getSavedWifi() {
  //check for stored credentials
  if(SPIFFS.exists("/config.json")){
    const char * _ssid = "", *_pass = "";
    File configFile = SPIFFS.open("/config.json", "r");
    
    if(configFile){
      Serial.println("");
      Serial.println("Config Found");
      size_t size = configFile.size();
      std::unique_ptr<char[]> buf(new char[size]);
      configFile.readBytes(buf.get(), size);
      configFile.close();

      DynamicJsonDocument doc(512);
      DeserializationError error = deserializeJson(doc, buf.get());
      if (!error) {
        _ssid = doc["ssid"];
        _pass = doc["password"];
        
        WiFi.mode(WIFI_STA);
        WiFi.begin(_ssid, _pass);
        
        unsigned long startTime = millis();
        while (WiFi.status() != WL_CONNECTED) {
          delay(500);
          Serial.print(".");
          if ((unsigned long)(millis() - startTime) >= 5000) break;
        }
      }
    }
  }
}


void basicResponse(bool success) {
  StaticJsonDocument<500> doc;
  doc["success"] = success;
  doc["message"] = success? "Success" : "Failed";
  doc["action"] = "";
  doc.createNestedObject("data");

  String response;
  serializeJsonPretty(doc, response);
  server.send(200, "text/json", response);
}
