void setup() {
  Serial.begin(9600);
  // Mandatory at first line
  setupOTA();
  setupMqtt();
  setupSensorTemperature();

  // simulate diff board
  setupRelays();
}

bool off = true;
void loop() {
  // Mandatory at first line
  handleOTAIfAny();
  
  listenMqttSubscribtion();
  listenTemperature();
  
  // simulate diff board
}
