#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char *mqtt_broker = "jogjaland.net";
const int mqtt_port = 1883;

const char *topic_temperature = "jogjaland_temperature";

// must be in different board
const char *topic_ac = "jogjaland_ac";
const char *topic_trap = "jogjaland_trap";
const char *topic_sprinkler = "jogjaland_sprinkler";
const char *topic_alarm = "jogjaland_alarm";


WiFiClient espClient;
PubSubClient mqttClient(espClient);

void setupMqtt() {
  mqttClient.setServer(mqtt_broker, mqtt_port);
  mqttClient.setCallback(mqttCallback);
}

void listenMqttSubscribtion() {
  if (!mqttClient.connected()) {
        connectMqtt();
  }
  mqttClient.loop();
}

void connectMqtt() {
  while (!mqttClient.connected()) {
     Serial.println("Connecting to public emqx mqtt broker.....");
     if (mqttClient.connect("esp8266-client")) {
         Serial.println("Public emqx mqtt broker connected");
         subscribeTopic(const_cast<char *>(topic_temperature));
     } else {
         Serial.print("failed with state ");
         Serial.print(mqttClient.state());
         delay(2000);
     }
  }
}

void disconnectMqtt() {
  mqttClient.disconnect();
}

void mqttCallback(char *topic, byte *payload, unsigned int length) {      
   if(strcmp(topic_temperature, topic) == 0) {
      char* full = new char[length];
      for (int i = 0; i < length; i++) {
         full[i] = payload[i];
      }

      double temp = strtod(full,NULL);
      Serial.println(temp);
   }
}

void subscribeTopic(char *topic) {
  mqttClient.subscribe(topic);
}

void unsubscribeTopic(char *topic) {
  mqttClient.unsubscribe(topic);
}

void publishMqtt(char *topic, char *payload) {
  mqttClient.publish(topic, payload);
  Serial.print("uploaded: ");
  Serial.println(payload);
}

void publishCelciusTemperature(char *temperature) {
  char* topic = const_cast<char*>(topic_temperature);
  publishMqtt(topic, temperature);
}

void publishAc(char * state) {
  char* topic = const_cast<char*>(topic_ac);
  publishMqtt(topic, state);
}

void publishTrap(char * state) {
  char* topic = const_cast<char*>(topic_trap);
  publishMqtt(topic, state);
}

void publishSprinkler(char * state) {
  char* topic = const_cast<char*>(topic_sprinkler);
  publishMqtt(topic, state);
}

void publishAlarm(char * state) {
  char* topic = const_cast<char*>(topic_alarm);
  publishMqtt(topic, state);
}
