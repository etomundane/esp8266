const int ac = D5;
const int trap = D6;
const int sprinkler = D7;
const int alarm = D8;

char* ON = const_cast<char*>("1");
char* OFF = const_cast<char*>("0");

void setupRelays() {
  pinMode(ac, OUTPUT);
  pinMode(trap, OUTPUT);
  pinMode(sprinkler, OUTPUT);
  pinMode(alarm, OUTPUT);
}

void resetRelays() {
  digitalWrite(ac, LOW);
  digitalWrite(trap, LOW);
  digitalWrite(sprinkler, LOW);
  digitalWrite(alarm, LOW);

  publishAc(OFF);
  publishTrap(OFF);
  publishSprinkler(OFF);
  publishAlarm(OFF);
}

void turnAc(bool turnOn) {
  digitalWrite(ac, turnOn ? HIGH : LOW);
  publishAc(turnOn ? ON : OFF);
}

void turnTrap(bool turnOn) {
  digitalWrite(trap, turnOn ? HIGH : LOW);
  publishTrap(turnOn ?  ON : OFF);
}

void turnSprinkler(bool turnOn) {
  digitalWrite(sprinkler, turnOn ? HIGH : LOW);
  publishSprinkler(turnOn ?  ON : OFF);
}

void turnAlarm(bool turnOn) {
  digitalWrite(alarm, turnOn ? HIGH : LOW);
  publishAlarm(turnOn ?  ON : OFF);
}
