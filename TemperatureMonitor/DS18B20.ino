#include <OneWire.h>
#include <DallasTemperature.h>

// DS1820 Temperature Monitor
const int oneWireBus = D4;     
OneWire oneWire(oneWireBus);
DallasTemperature sensorTemperature(&oneWire);

void setupSensorTemperature() {
  sensorTemperature.begin();
}

float lastTemp = 0.0;
void listenTemperature() {
  float temp = getCelciusTemperature();
  if(temp != lastTemp) {
    lastTemp = temp;
    
    char temperature[5];                
    sprintf(temperature, "%.2f", lastTemp);
    publishCelciusTemperature(temperature);

    if (temp < 30) {
        resetRelays();
      } else {
        turnAc(temp >= 30 && temp < 50);
        turnTrap(temp >= 50);
        turnSprinkler(temp >= 100);
        turnAlarm(temp >= 70);
      }
  }
}

float getCelciusTemperature() {
  sensorTemperature.requestTemperatures(); 
  return sensorTemperature.getTempCByIndex(0);
}

float getFahrenheitTemperature() {
  sensorTemperature.requestTemperatures(); 
  return sensorTemperature.getTempFByIndex(0);
}
