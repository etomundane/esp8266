void setup() {
  Serial.begin(9600);
  setupOTA();
  pinMode(LED_BUILTIN, OUTPUT);
} 

void loop() {
  handleOTAIfAny();
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  delay(100);
}
