#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <ArduinoJson.h>

ESP8266WebServer server;
char* ssid = "YOUR_SSID"; //not used
char* password = "abc12345";
char* mySsid = "Mcu";

IPAddress local_ip(192,168,11,11);
IPAddress gateway(192,168,11,1);
IPAddress netmask(255,255,255,0);

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  SPIFFS.begin();

  wifiConnect();

  server.on("/toggle",toggleLED);
  server.on("/setup", HTTP_POST, handleSettingsUpdate);
  server.on("/clear", removeSettings);
  
  server.begin();
}

void loop() {
  server.handleClient();
}

void basicResponse(bool success) {
  StaticJsonDocument<500> doc;
  doc["success"] = success;
  doc["message"] = success? "Success" : "Failed";
  doc["action"] = "";
  doc.createNestedObject("data");

  String response;
  serializeJsonPretty(doc, response);
  server.send(200, "text/json", response);
}

void handleSettingsUpdate() {
  String data = server.arg("plain");
  DynamicJsonDocument doc(512);
  DeserializationError error = deserializeJson(doc, data);
  if (error) return;

  File configFile = SPIFFS.open("/config.json", "w");
  serializeJson(doc, configFile);  
  configFile.close();
  
  basicResponse(true);
  delay(500);
  
  wifiConnect();
}

void wifiConnect() {
  //reset networking
  WiFi.softAPdisconnect(true);
  WiFi.disconnect();          
  delay(1000);
  
  //check for stored credentials
  if(SPIFFS.exists("/config.json")){
    const char * _ssid = "", *_pass = "";
    File configFile = SPIFFS.open("/config.json", "r");
    if(configFile){
      size_t size = configFile.size();
      std::unique_ptr<char[]> buf(new char[size]);
      configFile.readBytes(buf.get(), size);
      configFile.close();

      DynamicJsonDocument doc(512);
      DeserializationError error = deserializeJson(doc, buf.get());
      if (!error) {
        _ssid = doc["ssid"];
        _pass = doc["password"];
        
        WiFi.mode(WIFI_STA);
        WiFi.begin(_ssid, _pass);
        unsigned long startTime = millis();
        while (WiFi.status() != WL_CONNECTED) {
          delay(500);
          Serial.print(".");
          digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
          if ((unsigned long)(millis() - startTime) >= 5000) break;
        }
      }
    }
  }

  if (WiFi.status() == WL_CONNECTED) {
    Serial.println("Connected to Network");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    digitalWrite(LED_BUILTIN,HIGH);
    delay(3000);
    } else {
    WiFi.mode(WIFI_AP);
    WiFi.softAPConfig(local_ip, gateway, netmask);
    WiFi.softAP(mySsid, password); 
    digitalWrite(LED_BUILTIN,LOW);      
  }
  
  Serial.println("");
  WiFi.printDiag(Serial);
}

void toggleLED() {
  digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
  server.send(204,"");
}

void removeSettings() {
  Serial.println("");
  String result = SPIFFS.remove("/config.json") ? "Disk Cleared" : "Failed Clear Disk";
  Serial.println(result);
  wifiConnect();
}
