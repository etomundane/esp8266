
int leds[] = {D0, D1, D2, D3, D4};
int ledsSize = sizeof(leds)/sizeof(int);
bool even = ledsSize % 2 == 0;

void setup() {
  for(int x = 0; x < ledsSize; x++){
    pinMode(leds[x],OUTPUT);
  }
}

void loop() {
  fromCenter();
}
