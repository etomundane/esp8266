#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char *mqtt_broker = "jogjaland.net";
const int mqtt_port = 1883;

const char *topic_temperature = "jogjalandtemperature";
char *topic_switch = "jogjalandswitch";

WiFiClient espClient;
PubSubClient mqttClient(espClient);

// led callback
char led;

void setupMqtt() {
  mqttClient.setServer(mqtt_broker, mqtt_port);
  mqttClient.setCallback(mqttCallback);
}

void listenMqttSubscribtion() {
  if (!mqttClient.connected()) {
        connectMqtt();
  }
  mqttClient.loop();
}

void connectMqtt() {
  while (!mqttClient.connected()) {
     Serial.println("Connecting to public emqx mqtt broker.....");
     if (mqttClient.connect("esp8266-client")) {
         Serial.println("Public emqx mqtt broker connected");
     } else {
         Serial.print("failed with state ");
         Serial.print(mqttClient.state());
         delay(2000);
     }
  }

  // all subscription here
  subscribeLedControl();
}

void disconnectMqtt() {
  mqttClient.disconnect();
}

void mqttCallback(char *topic, byte *payload, unsigned int length) {
  char* full = new char[length];
  for (int i = 0; i < length; i++) {
     full[i] = payload[i];
  }
  Serial.println(full);
  
  if(strcmp(topic_switch, topic) == 0) {
    if ((char)payload[0] == '1') {
      turnOnLED();
    } else if ((char)payload[0] == '2') {
      fromLeft();
    } else if ((char)payload[0] == '3') {
       fromRight();
    } else if ((char)payload[0] == '4') {
       fromCenter();
    } else {
      turnOffLED();
    }
  }
}

void subscribeTopic(char *topic) {
  mqttClient.subscribe(topic);
  Serial.print("Subscribed to: ");
  Serial.println(topic);
}

void unsubscribeTopic(char *topic) {
  mqttClient.unsubscribe(topic);
}

void publishMqtt(char *topic, char *payload) {
  mqttClient.publish(topic, payload);
  Serial.print("uploaded: ");
  Serial.println(payload);
}


//// USE CASE
void publishCelciusTemperature(char *temperature) {
  char* topic = const_cast<char*>(topic_temperature);
  publishMqtt(topic, temperature);
}

void subscribeLedControl() {
  subscribeTopic(topic_switch);
}
