void setup() {
  Serial.begin(9600);
  
  // Mandatory at first line
  setupOTA();
  setupLED();

  setupMqtt();

}

void loop() {
  // Mandatory at first line
  handleOTAIfAny();

  listenMqttSubscribtion();
}
