int leds[] = {D0, D1, D2, D3, D4, D5};
int ledsSize = sizeof(leds)/sizeof(int);

void setupLED() {
  for(int x = 0; x < ledsSize; x++){
    pinMode(leds[x],OUTPUT);
  }
}

void turnOffLED() {
  for(int i = 0; i < ledsSize; i++){
    digitalWrite(leds[i],LOW);
  }
}

void turnOnLED() {
  for(int i = 0; i < ledsSize; i++){
    digitalWrite(leds[i],HIGH);
  }
}

void fromRight() {
//  do {
    for(int i = 0; i < ledsSize; i++){
      digitalWrite(leds[i],HIGH);
      delay(50);
      digitalWrite(leds[i],LOW);
    }
    
//    delay(200);
//    resetClient();
//  } while (currentAction == ACTION_RIGHT);
}

void fromLeft() {
//  do {
    for(int i = ledsSize; i >= 0; i--){
      digitalWrite(leds[i],HIGH);
      delay(50);
      digitalWrite(leds[i],LOW);
    }
    
//    delay(200);
//    resetClient();
//  } while (currentAction == ACTION_LEFT);
}

void fromCenter() {
//  do {
    centerRunning();
//    delay(200);
//    resetClient();
//  } while (currentAction == ACTION_CENTER);
}

void centerRunning() {
  int loops = ledsSize / 2;
  bool odd = ledsSize % 2 != 0;
  
  if(odd) {
    loops = (ledsSize / 2) + 1;
  }

  for(int i = 0; i < loops; i++){
      int i1 = loops + i;
      int i2 = loops - (i+1);
  
      if(odd) {
        i1 = i--;
      }
          
      digitalWrite(leds[i1],HIGH);
      digitalWrite(leds[i2],HIGH);
      delay(100);
      digitalWrite(leds[i1],LOW);
      digitalWrite(leds[i2],LOW);
  }
}
