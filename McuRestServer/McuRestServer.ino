#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ArduinoJson.h>
 
const char* ssid = "Kost Bu Rima";
const char* password = "bismillah";

ESP8266WebServer server(80);

// Board parameter
const int ACTION_LEFT = 1001;
const int ACTION_RIGHT = 1002;
const int ACTION_CENTER = 1003;

int leds[] = {D0, D1, D2, D3, D4, D5};
int ledsSize = sizeof(leds)/sizeof(int);
static int currentAction = 0;

void setup(void) {
  // Board output 
  for(int x = 0; x < ledsSize; x++){
    pinMode(leds[x],OUTPUT);
  }


  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
 
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
 
  // Activate mDNS this is used to be able to connect to the server
  // with local DNS hostmane esp8266.local
  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }
 
  startServer();
  
}
 
void loop(void) {
  String available = server.client() ? "available" : "unavailable";
  Serial.println(available);
}
