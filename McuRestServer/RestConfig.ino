
void startServer() {
  routingConfig();
  server.begin();
}

void resetClient() {
  server.client().stop();
  startServer();
  server.handleClient();
}

void handleNotFound() {
  server.send(404, "text/plain", "404: Not found"); 
}

void handleInvalidRequest() {
  server.send(400, "text/plain", "400: Invalid Request");
}

void routingConfig() {
    server.onNotFound(handleNotFound);
  
    server.on("/", HTTP_GET, []() {
        server.send(200, F("text/html"),
            F("Welcome, Bud!"));
    });
    
    server.on(F("/switchLed"), HTTP_POST, handleSwitchLed);
    server.on(F("/stopLed"), HTTP_POST, handleStopLed);
    server.on(F("/startLed"), HTTP_POST, handleStartLed);
}

void handleSwitchLed() {
  if(!server.hasArg("action") || server.arg("action") == NULL) {
     handleInvalidRequest();
  } else {
    int action = server.arg("action").toInt();
    if(action > 1000 && action < 1010) {
      basicResponse(true);
      currentAction = action;
      
    Serial.print("action: ");
    Serial.println(currentAction);
      switch(action) {
        case ACTION_LEFT: fromLeft(); break;
        case ACTION_RIGHT: fromRight(); break;
        case ACTION_CENTER: fromCenter(); break;
        default: turnOff();
      }
    } else {
      handleInvalidRequest();
    }
  }
}

void handleStopLed() {
  currentAction = 0;
  basicResponse(true);
  turnOff();
}

void handleStartLed() {
  currentAction = 0;
  basicResponse(true);
  turnOn();
}

void basicResponse(bool success) {
  StaticJsonDocument<500> doc;
  doc["success"] = success;
  doc["message"] = success? "Success" : "Failed";
  doc["action"] = "";
  doc.createNestedObject("data");

  String response;
  serializeJsonPretty(doc, response);
  server.send(200, "text/json", response);
}
