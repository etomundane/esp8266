package com.etomun.iot.esp8266;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.etomun.iot.R;
import com.etomun.iot.esp8266.util.BaseResponse;
import com.etomun.iot.esp8266.util.BoardApi;
import com.etomun.iot.esp8266.util.LedAction;
import com.etomun.iot.esp8266.util.RestClient;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusActivity extends AppCompatActivity {
    public static final String TAG = BusActivity.class.getSimpleName();

    private BoardApi api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus);
        api = RestClient.getClient().create(BoardApi.class);
    }

    public void turnLeft(View view) {
        api.switchLed(LedAction.ACTION_LEFT).enqueue(new Callback<BaseResponse<Void>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Void>> call, @NotNull Response<BaseResponse<Void>> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Void>> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    public void hazard(View view) {
        api.switchLed(LedAction.ACTION_HAZARD).enqueue(new Callback<BaseResponse<Void>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Void>> call, @NotNull Response<BaseResponse<Void>> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Void>> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    public void turnRight(View view) {
        api.switchLed(LedAction.ACTION_RIGHT).enqueue(new Callback<BaseResponse<Void>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Void>> call, @NotNull Response<BaseResponse<Void>> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Void>> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    public void turnOff(View view) {
        api.turnOff().enqueue(new Callback<BaseResponse<Void>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Void>> call, @NotNull Response<BaseResponse<Void>> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Void>> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    public void turnOn(View view) {
        api.turnOn().enqueue(new Callback<BaseResponse<Void>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Void>> call, @NotNull Response<BaseResponse<Void>> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Void>> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}