package com.etomun.iot.esp8266.util;

public interface LedAction {
    int ACTION_LEFT = 1001;
    int ACTION_RIGHT = 1002;
    int ACTION_HAZARD = 1003;
}
