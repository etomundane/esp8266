package com.etomun.iot.esp8266.util;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BaseResponse<DATA> {
    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("message")
    private String message;

    @SerializedName("action")
    private String action;

    @SerializedName("data")
    private DATA data;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getAction() {
        return action;
    }

    public DATA getData() {
        return data;
    }
}
