package com.etomun.iot.esp8266.util;

import com.etomun.iot.esp8266.SetupParam;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface McuSetup {
    @POST("/setup")
    Call<BaseResponse<Boolean>> setWifi(@Body SetupParam param);
}
