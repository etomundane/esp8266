package com.etomun.iot.esp8266;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.etomun.iot.R;
import com.etomun.iot.esp8266.util.BaseResponse;
import com.etomun.iot.esp8266.util.McuClient;
import com.etomun.iot.esp8266.util.McuSetup;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetupActivity extends AppCompatActivity {
    public static final String TAG = SetupActivity.class.getSimpleName();

    private EditText vSSID;
    private EditText vPassword;

    private McuSetup api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        vSSID = findViewById(R.id.ssid);
        vPassword = findViewById(R.id.password);

        api = McuClient.getClient().create(McuSetup.class);

        getCurrentSSID();
    }

    private void getCurrentSSID() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo;

        wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
            String name = wifiInfo.getSSID().replaceAll("\"", "");
            vSSID.setText(name);
            connectToESP();
        }
    }

    private void connectToESP() {
        String name = "Smart Device";
        String password = "abc12345";

        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = java.lang.String.format("\"%s\"", name);
        wifiConfig.preSharedKey = String.format("\"%s\"", password);

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int netId = wifiManager.addNetwork(wifiConfig);
        wifiManager.disconnect();
        wifiManager.enableNetwork(netId, true);
        wifiManager.reconnect();
    }

    public void setUpWifi(View view) {
        String ssid = vSSID.getText().toString();
        String password = vPassword.getText().toString();
        SetupParam param = new SetupParam(ssid, password);

        api.setWifi(param).enqueue(new Callback<BaseResponse<Boolean>>() {
            @Override
            public void onResponse(@NotNull Call<BaseResponse<Boolean>> call, @NotNull Response<BaseResponse<Boolean>> response) {
                Log.d(TAG, response.toString());
            }

            @Override
            public void onFailure(@NotNull Call<BaseResponse<Boolean>> call, @NotNull Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}