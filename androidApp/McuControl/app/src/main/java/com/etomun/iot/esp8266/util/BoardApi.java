package com.etomun.iot.esp8266.util;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface BoardApi {
    @FormUrlEncoded
    @POST("/switchLed")
    Call<BaseResponse<Void>> switchLed(@Field("action") int action);

    @POST("/stopLed")
    Call<BaseResponse<Void>> turnOff();

    @POST("/startLed")
    Call<BaseResponse<Void>> turnOn();

    @POST("/temperatureMonitor")
    Call<BaseResponse<Double>> temperatureMonitor();
}
