package com.etomun.iot.esp8266.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class McuClient {
    public static final String BASE_URL = "http://192.168.11.11";
    private static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
