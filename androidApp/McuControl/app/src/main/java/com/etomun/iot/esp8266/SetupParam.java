package com.etomun.iot.esp8266;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetupParam {

    @SerializedName("ssid")
    @Expose
    private String ssid;
    @SerializedName("password")
    @Expose
    private String password;

    public SetupParam(String ssid, String password) {
        this.ssid = ssid;
        this.password = password;
    }
}